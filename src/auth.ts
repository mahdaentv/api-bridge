import { AccessType, AuthConfig, AuthProfile, GatewayRequest, Route } from './interface';
import { decode, sign, verify } from 'jsonwebtoken';
import { AxiosResponse } from 'axios';
import { GatewayMiddlware, MiddlewareError } from './gateway';

/**
 * Interface to be implemented to a class that will be used as an auth handler.
 */
export abstract class Auth {
  /**
   * Method to create an authentication middleware.
   */
  abstract middleware(): GatewayMiddlware;

  /**
   * Method to create a route definition that will handle login request and create a token. A `jwt` field
   * will be added to the `response.data` after the login complete.
   * @param path
   */
  abstract route?(): Route | Route[];
}

/**
 * A class that extends Auth.
 */
export type AuthConstructor = { new(...args: any[]): Auth };

/**
 * Error information about an Unauthorized access error.
 */
export class AccessError extends MiddlewareError {
  constructor() {
    super(401, 'Unauthorized access.');
  }
}

/**
 * Error information about a bad request related to authentication.
 */
export class BadAuthRequest extends MiddlewareError {
  constructor() {
    super(400, 'Bad authorization request.');
  }
}

/**
 * Basic JWT authentication for the Gateway.
 */
export class JWTAuth extends Auth {
  /**
   * Create new authentication instance.
   * @param configs Configuration to define how the authentication works.
   */
  constructor(protected configs: AuthConfig) {
    super();
  }

  /**
   * Method to create a Gateway middleware.
   */
  public middleware(): GatewayMiddlware {
    return (req, route) => this.httpAuth(req, route);
  }

  /**
   * Method to create a route definition that will handle login request and create a token. A `jwt` field
   * will be added to the `response.data` after the login complete.
   * @param path
   */
  public route(path?: string): Route {
    if (!path && !this.configs.authenticationPath) {
      throw new Error('No authentication path defined.');
    }

    return {
      path: path || this.configs.authenticationPath,
      methods: ['post'],
      direct: true,
      beforeRequest: (req: GatewayRequest) => {
        if (this.configs.beforeLogin) {
          this.configs.beforeLogin(req);
        }
      },
      transformResponse: async (req: GatewayRequest, res: AxiosResponse) => {
        const profile = this.configs.createProfile ? this.configs.createProfile(res) : res.data;
        res.data = { ...res.data, jwt: this.createToken(profile) };

        if (this.configs.afterLogin) {
          await this.configs.afterLogin(req, res);
        }
      }
    }
  }

  /**
   * Internal method to sign a profile and create a token.
   * @param profile Object to be included in the token.
   */
  protected createToken(profile?: AuthProfile): string {
    return sign(profile, this.configs.jwtSecret);
  }

  /**
   * Internal method that will authenticate the http requests.
   * @param req Express request object.
   * @param route The matching route with the request.
   */
  protected httpAuth(req: GatewayRequest, route?: Route): void | MiddlewareError {
    const { authorization, Authorization } = req.headers;

    if (route) {
      if (route.security && route.security.type === AccessType.ALLOW_ALL) {
        return;
      }

      if (route.security && route.security.type === AccessType.DENNY_ALL) {
        return new AccessError();
      }
    }

    if (authorization || Authorization) {
      const token = (authorization || Authorization as string).replace('Bearer ', '');
      const verified = verify(token, this.configs.jwtSecret);

      if (verified) {
        const decoded = decode(token) as AuthProfile;
        req.authProfile = decoded;

        if (this.configs.transformRequest) {
          this.configs.transformRequest(req, decoded);
        }

        if (route && route.security) {
          return this.verifyAccess(req, route, decoded);
        }
      } else {
        return new AccessError();
      }
    } else {
      return new BadAuthRequest();
    }
  }

  /**
   * Internal method that will verify the access to the request. This method will be called
   * when the request has a matching route, and the route has a security definition.
   * @param req Express request object.
   * @param route Matching route with the request.
   * @param profile Auth profile that included on the token.
   */
  protected verifyAccess(req: GatewayRequest, route: Route, profile: AuthProfile): void | MiddlewareError {
    const { type, roles } = route.security;

    if (AccessType.AUTHENTICATED) {
      return;
    }

    if (type === AccessType.HAS_ROLE) {
      if ((profile.roles || []).join(',') !== (roles || []).join(',')) {
        return new AccessError();
      }
    }

    if (type === AccessType.HAS_ANY_ROLE) {
      let hasRole = false;
      for (const role of (roles || [])) {
        if ((profile.roles || []).includes(role)) {
          hasRole = true;
        }
      }

      if (!hasRole) {
        return new AccessError();
      }
    }
  }
}
