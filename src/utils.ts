import { Fields, GatewayRequest } from './interface';
import * as _ from 'lodash';

/**
 * Select specific fields from the give data.
 * @param data Object or Array contains Object to select the fields from.
 * @param fields Field selector.
 *
 * @example
 * ```typescript
 * const data = { a: 1, b: 2, c: { a: 1, b: 2 } };
 * const result1 = selectFields(data, { a:true, c: { b: true } }); // Result: { a: 1, c: { b: 2 } };
 * const result2 = selectFields(data, { 'c.b':true }); // Result: { c: { b: 2 } };
 * const result3 = selectFields(data, { b: false, c: false } // Result: { a: 1, c: { a: 1 } };
 * ```
 */
export function selectFields<T>(data: object | object[], fields: Fields<T>): object | object[] {
  if (Array.isArray(data)) {
    return data.map(d => selectFields(d, fields));
  } else {
    const selected: object = {};

    for (const [field, include] of Object.entries(fields)) {
      const value = _.get(data, field);
      if (include === true && value) {
        _.set(selected, field, value);
      } else if (include === false) {
        _.unset(data, field);
      } else if (toString.call(include) === '[object Object]' && value) {
        _.set(selected, field, selectFields(value, include));
      }
    }

    if (Object.keys(selected).length) {
      return selected;
    } else {
      return data;
    }
  }
}

/**
 * Filter data fields to match with the requested fields.
 * @param data Data to be filtered.
 * @param req Request object.
 */
export function filterFields(data: object | object[], req: GatewayRequest) {
  if (req.query.filter && req.query.filter.fields) {
    return selectFields(data, req.query.filter.fields);
  } else {
    return data;
  }
}
