import { Gateway } from './gateway';
import { AxiosResponse } from 'axios';

const {
  API_URL,
  SERVER_SECRET = 'nobodyknows',
  SERVER_PORT = 3000,
  JWT_SECRET,
  AUTH_PATH,
} = process.env as any;

if (!API_URL) {
  throw new Error('Remote API_URL required.');
}

const gateway = new Gateway({ secret: SERVER_SECRET, eventHook: '/push-event' });
gateway.addRemote({
  name: 'default',
  baseURL: API_URL,
});

if (JWT_SECRET && AUTH_PATH) {
  gateway.addAuth({
    jwtSecret: JWT_SECRET,
    authenticationPath: AUTH_PATH,
    createProfile: (res: AxiosResponse) => {
      return { id: res.data.id };
    },
  });
}

export function serve() {
  gateway
    .start(parseInt(SERVER_PORT))
    .then(() => {
      console.log(`Server listening on http://localhost:${SERVER_PORT}.`);
    });
}
