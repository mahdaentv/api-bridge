import { Response } from 'express';

import { BasicRemote, RemoteConstructor, Remote } from './remote';
import {
  AuthConfig,
  GatewayConfig,
  GatewayRequest,
  GatewayResponseHandler,
  RemoteConfig,
  Route
} from './interface';
import { AxiosError, AxiosResponse } from 'axios';
import { Auth, AuthConstructor, JWTAuth } from './auth';
import { RestTime } from './rest-time';
import { Logger } from 'event-bridge';

/**
 * Gate middleware to handle the request before forwarded to the remote.
 */
export type GatewayMiddlware = (req: GatewayRequest, route?: Route) => void | MiddlewareError | Promise<void | MiddlewareError>;

/**
 * Error info about the middleware error, to terminate the client connection.
 */
export class MiddlewareError extends Error {
  /**
   * Create new MiddlewareError instance.
   * @param code Error code.
   * @param message Error message.
   */
  constructor(public code: number, message: string) {
    super(message);
  }
}

/**
 * API Gateway that built with event publisher to give a real time support.
 */
export class Gateway {
  /** Default remote instance to forward the requests. **/
  private defaultRemote: Remote;
  /** Registered remotes. **/
  private readonly remotes: {
    [name: string]: Remote
  } = {};
  /** Registered routes. **/
  private readonly routes: {
    [path: string]: Route;
  } = {};
  /** Registered gateway middlewares. **/
  public readonly middlewares: GatewayMiddlware[] = [];

  /** Express instance. **/
  public readonly app: RestTime;

  /**
   * Create new Gateway instance.
   * @param configs Gateway configs.
   */
  constructor(private configs: GatewayConfig) {
    this.app = new RestTime(configs);

    if (configs.eventHook) {
      this.app.http.post(configs.eventHook, (req: GatewayRequest, res: Response) => {
        const { path, type, data } = req.body;

        if (!path || !type || !data) {
          res.status(400);
          res.send({ errors: ['Bad request. Event must have { path, type, data }!'] });
          return;
        }

        if (req.query.secret !== configs.secret) {
          res.status(400);
          res.send({ errors: ['Bad request. Query param ?secret={{GATEWAY_SECRET}} is required.'] });
          return;
        }

        this.app.emit<any>({ path, type, data });
        res.send('Published.');
      });
    }
  }

  /**
   * Add remote instance. Remote instance will forward the requests to the API server.
   * @param remoteInstance [[Remote]] instance, remote configs, or class that extends [[Remote]].
   * @param configs Remote configs, required if the first argument is a class.
   */
  public addRemote(remoteInstance: Remote): this;
  public addRemote(remoteConfigs: RemoteConfig): this;
  public addRemote(RemoteClass: RemoteConstructor, configs: RemoteConfig): this;
  public addRemote(remote: RemoteConstructor | Remote | RemoteConfig, configs?: RemoteConfig): this {
    if (remote instanceof Remote) {
      this.remotes[remote.configs.name] = remote;
      if (remote.configs.name === 'default' || remote.configs.default) {
        this.defaultRemote = remote;
      }
    } else if (typeof remote === 'function') {
      this.remotes[configs.name] = new remote(configs);
      if (configs.name === 'default' || configs.default) {
        this.defaultRemote = this.remotes[configs.name];
      }
    } else {
      this.remotes[remote.name] = new BasicRemote(remote);
      if (remote.name === 'default' || remote.default) {
        this.defaultRemote = this.remotes[remote.name];
      }
    }

    if (!this.defaultRemote) {
      for (const [, instance] of Object.entries(this.remotes)) {
        this.defaultRemote = instance;
      }
    }

    return this;
  }

  /**
   * Add authentication instance to authenticate the requests and handle login request.
   * @param authInstance [[Auth]] instance, [[AuthConfig]] or a class that extends [[Auth]].
   * @param configs Auth configs, required if the first argument is a class.
   */
  public addAuth(authInstance: Auth): this;
  public addAuth(authConfigs: AuthConfig): this;
  public addAuth(AuthClass: AuthConstructor, configs: AuthConfig): this;
  public addAuth(auth: AuthConstructor | Auth | AuthConfig, configs?: AuthConfig): this {
    if (typeof auth === 'function') {
      this.addAuth(new auth(configs));
    } else if (auth instanceof Auth) {
      this.use(auth.middleware());
      if (auth.route) {
        this.route(auth.route());
      }
    } else {
      this.addAuth(new JWTAuth(configs));
    }

    return this;
  }

  /**
   * Add middleware to handle http requests and websocket requests, before forwarded to the remote.
   * Typically you must define the same handler for both http and websocket, like prevent unauthorized
   * access to an endpoint on both http and websocket request.
   * @param http Function to handle http requests.
   * @param socket Function to handle websocket requests.
   */
  public use(middlware: GatewayMiddlware) {
    this.middlewares.push(middlware);
  }

  /**
   * Register a route definition. Registering custom route will allow you to have more control to the
   * endpoint such bypass the middleware, set the target remote, modify the response data, etc.
   * @param route A route definition.
   */
  public route(route: Route | Route[]): this {
    if (Array.isArray(route)) {
      route.forEach(r => this.route(r));
    } else {
      this.routes[route.path] = route;

      for (const method of route.methods) {
        if (typeof this.app[method] === 'function') {
          (this.app[method] as Function)(route.path, async (req: GatewayRequest, res: Response) => {
            for (const [key, value] of Object.entries(req.query) as [string, any]) {
              if (
                typeof value === 'string' && (
                  (value.startsWith('{') && value.endsWith('}')) ||
                  (value.startsWith('[') && value.endsWith(']'))
                )
              ) {
                try {
                  req.query[key] = JSON.parse(value);
                } catch (error) {}
              }
            }
            try {
              if (route.beforeRequest) {
                await route.beforeRequest(req);
              }

              if (route.direct) {
                return await this.directRequest(req, res, route.transformResponse);
              } else {
                return await this.request(req, res, route.transformResponse);
              }
            } catch (error) {
              this.error(res, error);
            }
          });
        }
      }
    }

    return this;
  }

  /**
   * Start the Gateway server. When starting the server, it'll add a route-all method to handle any requests.
   * To disable adding route-all, add `routedOnly` on the [[GatewayConfig]].
   * @param port Port to listen the server.
   */
  public start(port: number): Promise<void> {
    this.routeAll();
    return new Promise(resolve => {
      this.app.listen(port, resolve);
    });
  }

  /**
   * Internal method to create a request and process the middleware before sending it to the remote.
   * @param req Express request object.
   * @param res Express response object.
   */
  protected async request(req: GatewayRequest, res: Response, handler?: GatewayResponseHandler): Promise<void> {
    for (const middleware of this.middlewares) {
      const err = await middleware(req);
      if (err) {
        res.status(err.code);
        res.send({ errors: [err.message] });
        return;
      }
    }

    const response = await this.forward(req, res);
    if (response) {
      if (typeof handler === 'function') {
        await handler(req, response);
      }
      await this.response(req, res, response);
    }
  }

  /**
   * Internal method to create a request without processing the middleware and send it directly to the remote.
   * @param req Express request object.
   * @param res Express response object.
   * @param handler Function to handle the response data returned from the remote.
   */
  protected async directRequest(req: GatewayRequest, res: Response, handler?: GatewayResponseHandler): Promise<void> {
    const response = await this.forward(req, res);
    if (response) {
      if (typeof handler === 'function') {
        await handler(req, response);
      }
      await this.response(req, res, response);
    }
  }

  /**
   * Internal method to forward the request to the remote.
   * @param req Express request object.
   * @param res Express response object.
   */
  protected async forward(req: GatewayRequest, res: Response): Promise<AxiosResponse> {
    const remote = this.switchRemote(req);

    if (remote) {
      try {
        return await remote.request(req);
      } catch (error) {
        this.error(res, error);
      }
    } else {
      res.status(500);
      res.send({ errors: ['No remote to handle the requests.'] });
    }
  }

  /**
   * Internal method to send a response to the client.
   * @param req Express request object.
   * @param res Express response object.
   * @param response Response data from the Remote.
   */
  protected async response(req: GatewayRequest, res: Response, response: AxiosResponse): Promise<void> {
    const { status, data, headers = {} } = response;

    res.status(status);
    for (const [key, val] of Object.entries(headers) as [string, any]) {
      res.setHeader(key, val);
    }
    res.send(data);

    if (['post', 'put', 'patch', 'delete'].includes(req.method.toLowerCase())) {
      if (req.method.toLowerCase() === 'delete' && !data.id) {
        const paths = req.path.split('/');
        const id = paths.pop();

        if (id && /[-]+/.test(id) && id.length > 30) {
          this.app.emit({ type: req.method.toLowerCase() as any, path: req.path, data: { id } });
        }
      } else {
        this.app.emit({ type: req.method.toLowerCase() as any, path: req.path, data });
      }
    }
  }

  /**
   * Internal method to handle request error.
   * @param res Express response object.
   * @param error Error instance.
   */
  protected error(res: Response, error: AxiosError) {
    Logger.error(error.message, error);
    if (error.response) {
      res.status(error.response.status);
      res.send(error.response.data);
    } else {
      res.status(500);
      res.send({ errors: [error.message] });
    }
  }

  /**
   * Internal method to add a route-all handling. By calling this method, eny requests will be
   * forwarded to the remote no matter it's exist or not. If you have a custom routes, make sure
   * you register them before starting the server.
   */
  protected routeAll() {
    if (!this.configs.routedOnly) {
      this.route({
        path: '/**',
        methods: ['get', 'post', 'put', 'patch', 'delete']
      });
    }
  }

  /**
   * Internal method to switch which Remote should be used to foward the request.
   * @param req Express request object.
   */
  protected switchRemote(req: GatewayRequest): Remote {
    const { path } = req;

    if (this.routes[path]) {
      const { remote } = this.routes[path];
      if (remote && this.remotes[remote]) {
        return this.remotes[remote];
      }
    } else {
      const remote = req.headers['x-remote'] as string;
      if (remote && this.remotes[remote]) {
        return this.remotes[remote];
      }
    }

    for (const [name, remote] of Object.entries(this.remotes)) {
      if (typeof remote.switch === 'function' && remote.switch(req)) {
        return remote;
      }
      if (typeof remote.configs.switch === 'function' && remote.configs.switch(req)) {
        return remote;
      }
    }

    return this.defaultRemote;
  }
}
