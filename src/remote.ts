import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, Method } from 'axios';
import { GatewayRequest, RemoteConfig } from './interface';

/**
 * Interface to be implemented to a class that will be used as a
 * remote handler. Remote handler will forward the requests to the
 * remote server.
 */
export abstract class Remote {
  /** Object contains remote configurations. **/
  public configs: RemoteConfig;

  /**
   * Forward express request to the server.
   * @param req Express request object.
   */
  abstract request(req: GatewayRequest): Promise<AxiosResponse>;

  /**
   * Optional method to switch to this remote based on the request.
   * @param req Express request object.
   */
  abstract switch?(req: GatewayRequest): boolean | void;

  /**
   * Optional method to transform the response.
   * @param req Express request object.
   * @param res Axios response object.
   */
  abstract transformResponse?(req: GatewayRequest, res: AxiosResponse): void | Promise<void>;
}

export type RemoteConstructor = { new(...args: any[]): Remote };

/**
 * Basic request handler that will simply forward the requests
 * to the destination URL.
 */
export class BasicRemote extends Remote {
  /** Axios instance to forward the request to the server. **/
  protected http: AxiosInstance;

  /**
   * Create new remote instance.
   * @param configs Remote configs.
   */
  constructor(public configs: RemoteConfig) {
    super();
    const { baseURL, headers } = configs;
    this.http = axios.create({ baseURL, headers });
  }

  /**
   * Forward express request to the server.
   * @param req Express request object.
   */
  public async request(req: GatewayRequest): Promise<AxiosResponse> {
    const config = await this.createConfig(req);
    const response = await this.http.request(config);

    if (this.transformResponse) {
      await this.transformResponse(req, response);
    }

    if (this.configs.transformResponse) {
      await this.configs.transformResponse(req, response);
    }

    return response;
  }

  /**
   * Optional method to switch to this remote based on the request.
   * @param req Express request object.
   */
  public switch?(req: GatewayRequest): boolean | void;

  /**
   * Optional method to transform the response.
   * @param req Express request object.
   * @param res Axios response object.
   */
  public transformResponse?(req: GatewayRequest, res: AxiosResponse): Promise<void>;

  /**
   * Create axios request config, by modifying the incomming request.
   * @param req Express request object.
   */
  protected async createConfig(req: GatewayRequest): Promise<AxiosRequestConfig> {
    if (this.configs.createConfig) {
      return this.configs.createConfig(req);
    } else {
      const headers = { ...req.headers };

      delete headers.host;
      delete headers.connection;

      return {
        url: req.originalUrl,
        method: req.method as Method,
        data: req.body,
        headers: this.configs.headers || headers
      }
    }
  }
}
