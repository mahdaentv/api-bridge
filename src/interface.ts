import { ServerConfig } from 'event-bridge';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { Request } from 'express';
import { WhereFilter } from 'event-bridge';

/**
 * Default token profile.
 */
export interface AuthProfile {
  id: string;
  roles?: string[];
}

/**
 * Configuration to define how the JWT authenticator works.
 */
export interface AuthConfig {
  /** String to secure the jwt token. **/
  jwtSecret: string;
  /** Path to handle the login request, so the authenticator can create a JWT. **/
  authenticationPath?: string;
  /** Function to transform before login request. **/
  beforeLogin?: (req: GatewayRequest) => void;
  /** Function to create the token profile, after login to the remote. **/
  createProfile?: (res: AxiosResponse) => AuthProfile;
  /** Function to transform the response, after modified by the authenticator. **/
  afterLogin?: GatewayResponseHandler;
  /** Function to transform http authenticated request. If no request transformer, auth will add `authProfile` field to the request object. **/
  transformRequest?: (req: GatewayRequest, profile: AuthProfile) => void;
}

/**
 * Authentication type to define how the authenticator will
 * authenticate the requests.
 */
export enum AccessType {
  AUTHENTICATED,
  HAS_ANY_ROLE,
  HAS_ROLE,
  ALLOW_ALL,
  DENNY_ALL
}

/**
 * Configuration to define how the gateway works.
 */
export interface GatewayConfig extends ServerConfig {
  /**
   * Do not handle all URLs, and only response to the routed paths defined by using
   * the `.route()` method.
   */
  routedOnly?: boolean;
  /** Add a path to publish an event via http `POST` request. **/
  eventHook?: string;
  secret?: string;
}

/**
 * Request object that extends the express request. It's usefull to give more context
 * to the remote, to define the axios config.
 */
export interface GatewayRequest extends Request {
  /** Object contains authentication data, such id and token. **/
  authProfile: AuthProfile;
}

export type GatewayRequestHandler = (req: GatewayRequest) => void | Promise<void>;
/**
 * Funciton to transform the response object. This function should not return anything,
 * but replace the response data if needed.
 */
export type GatewayResponseHandler = (req: GatewayRequest, res: AxiosResponse) => void | Promise<void>;

/**
 * Available method type that can be handled by the express app.
 */
export type RouteMethod = 'post' | 'get' | 'put' | 'patch' | 'delete';

/**
 * Route definition to add a routing to the express app.
 */
export type Route = {
  /** Route path. **/
  path: string;
  /** Methods to add to the route. **/
  methods: Array<RouteMethod>;
  /** If true, the request will be forwarded to the remote directly, without passing the middlewares. **/
  direct?: boolean;
  /** Remote name to handle the request, if there is more than one remote. **/
  remote?: string;
  /** Security definition to authenticate the request. **/
  security?: RouteSecurity;
  /** Function to modify the request object, before the request forwarded to the remote. **/
  beforeRequest?: GatewayRequestHandler;
  /** Function to modify the response object, after the request forwarde to the remote. **/
  transformResponse?: GatewayResponseHandler;
}

/**
 * Security definition to tell the authenticator how to authenticate the requests.
 */
export type RouteSecurity = {
  /** Authentication type. **/
  type: AccessType;
  /** Data related to the authentication type. **/
  roles?: string[];
}

/**
 * Configuration to define how the BasicRemote class works.
 */
export interface RemoteConfig {
  /** Remote name. **/
  name: string;
  /** Remote URL (target server URL). **/
  baseURL: string;
  /** Mark the remote as the default remote. **/
  default?: boolean;
  /** Headers to add to the request. **/
  headers?: object;
  /** Function to create a request config. **/
  createConfig?: (req: GatewayRequest) => AxiosRequestConfig | Promise<AxiosRequestConfig>;
  /** Function to transform the response data. **/
  transformResponse?: (req: GatewayRequest, res: AxiosResponse) => void | Promise<void>;
  switch?: (req: GatewayRequest) => boolean | void;
}

export type GatewayQuery = {
  search?: string;
  filter?: GatewayQueryFilter;
}

export type GatewayQueryFilter = {
  limit?: number;
  page?: number;
  where?: WhereFilter | WhereFilter[];
  facets?: string[];
  relationships: { [key: string]: string };
}

export type Fields<T> = {
  [K in keyof T]?: boolean | Fields<T[K]>;
}

export type FieldSelector = Array<string | SubFieldSelector>;
export type SubFieldSelector = {
  key: string,
  fields: FieldSelector;
}
