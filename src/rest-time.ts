import {
  RequestMiddleware,
  Server,
  ServerConfig,
  ServerEvent,
  Request as SocketRequest,
  Response as SocketResponse,
} from 'event-bridge';
import express, {
  Express, Request as HttpRequest,
  RequestHandler as HttpHandler,
  Response as HttpResponse,
} from 'express';
import {
  createServer,
  Server as HttpServer
} from 'http';

export type Request<R> = HttpRequest & SocketRequest<R>;
export type Response<D> = HttpResponse & SocketResponse<D>;
export type Middleware<R, D> = (req: Request<R>, res: Response<D>) => Promise<void> | void;
export type RequestHandler<R, D> = (req: Request<R>, res: Response<D>, emit: Emitter<D>) => Promise<void> | void;
export type Emitter<D> = (event: ServerEvent<D>) => void;

/**
 * REST API Server that support request via HTTP and WebSocket.
 *
 * @example
 * ```typescript
 * const app = new RestTime(config);
 * app.post('/users', (req, res, emit) => {
 *   // Db transaction.
 *   res.send('OK');
 * });
 *
 * axios.post('/users', data); // => OK.
 * ws.send({type:'request', data: {method:'post', data}}); // => OK.
 *
 * ```
 */
export class RestTime {
  public readonly http: Express;
  public readonly wstp: Server;
  public readonly server: HttpServer

  constructor(config: ServerConfig) {
    this.http = express();
    this.server = createServer(this.http);
    this.wstp = new Server(this.server, config);
    this.http.use(express.json());
  }

  public use<R, D>(middleware: Middleware<R, R>): this {
    this.http.use(middleware);
    this.wstp.use(middleware);
    return this;
  }

  public useHttp(middleware: HttpHandler): this {
    this.http.use(middleware);
    return this;
  }

  public useWstp(middleware: RequestMiddleware): this {
    this.wstp.use(middleware);
    return this;
  }

  public all<R, D>(path: string, handler: RequestHandler<R, D>): this {
    const fn = this.createHandler(handler);
    this.http.all(path, fn);
    this.wstp.all(path, fn);
    return this;
  }

  public get<D>(path: string, handler: RequestHandler<null, D>): this {
    const fn = this.createHandler(handler);
    this.http.get(path, fn);
    this.wstp.get(path, fn);
    return this;
  }

  public post<R, D>(path: string, handler: RequestHandler<R, D>): this {
    const fn = this.createHandler(handler);
    this.http.post(path, fn);
    this.wstp.post(path, fn);
    return this;
  }

  public put<R, D>(path: string, handler: RequestHandler<R, D>): this {
    const fn = this.createHandler(handler);
    this.http.put(path, fn);
    this.wstp.put(path, fn);
    return this;
  }

  public patch<R, D>(path: string, handler: RequestHandler<R, D>): this {
    const fn = this.createHandler(handler);
    this.http.patch(path, fn);
    this.wstp.patch(path, fn);
    return this;
  }

  public delete<D>(path: string, handler: RequestHandler<null, D>): this {
    const fn = this.createHandler(handler);
    this.http.delete(path, fn);
    this.wstp.delete(path, fn);
    return this;
  }

  public options(path: string, handler: RequestHandler<null, null>): this {
    const fn = this.createHandler(handler);
    this.http.options(path, fn);
    this.wstp.options(path, fn);
    return this;
  }

  public emit<T>(event: ServerEvent<T>): void {
    this.wstp.emit(event);
  }

  public listen(port: number, handler?: () => void) {
    this.server.listen(port, handler);
  }

  protected createHandler<R, D>(handler: RequestHandler<R, D>) {
    return (req: Request<R>, res: Response<D>) => {
      return handler(req, res, (event) => {
        this.emit(event);
      });
    }
  }
}
