import algoliasearch, { SearchClient, SearchIndex } from 'algoliasearch';
import { SearchResponse, SearchOptions } from '@algolia/client-search';

import { Remote } from './remote';
import { GatewayQueryFilter, GatewayRequest, RemoteConfig } from './interface';
import { AxiosResponse } from 'axios';
import { WhereFilter } from 'event-bridge';

export interface AlgoliaConfig extends RemoteConfig {
  appId: string;
  appKey: string;
  prefix?: string;
  suffix?: string;
}

export interface AlgoliaRequest {
  query?: string;
  options?: SearchOptions;
}

export class Algolia extends Remote {
  private client: SearchClient;
  private indexes: {
    [key: string]: SearchIndex;
  } = {}

  constructor(public configs: AlgoliaConfig) {
    super();
    this.client = algoliasearch(configs.appId, configs.appKey);
  }

  public async request(req: GatewayRequest): Promise<AxiosResponse> {
    const index = this.initIndex(req.headers['x-index'] as string);
    const { query, options } = this.createRequest(req);
    console.log(query, options);
    const res = await index.search(query, options);
    const response = this.createResponse(res);

    if (this.transformResponse) {
      await this.transformResponse(req, response);
    }

    if (this.configs.transformResponse) {
      await this.configs.transformResponse(req, response);
    }

    return response;
  }

  public switch(req: GatewayRequest): boolean | void {
    return 'x-index' in req.headers;
  }

  public transformResponse?(req: GatewayRequest, res: AxiosResponse): void | Promise<void>;

  protected initIndex(name: string): SearchIndex {
    const { prefix, suffix } = this.configs;
    return this.client.initIndex(`${prefix || ''}${name}${suffix || ''}`);
  }

  protected createRequest(req: GatewayRequest): AlgoliaRequest {
    const { search, filter = {}, facets, relationRef } = req.query || {};

    if (typeof relationRef === 'object') {
      filter.relationships = relationRef;
    }

    return {
      query: search || '',
      options: this.createOptions(filter, facets)
    }
  }

  protected createOptions(filter: GatewayQueryFilter, facets?: string[]): SearchOptions {
    const options: any = {};

    if (facets) {
      options.facets = facets;
    }
    if (filter.where) {
      Object.assign(options, this.transformWhereFilter(filter.where));
    }

    if (filter.relationships) {
      if (!options.facetFilters) {
        options.facetFilters = [];
      }

      for (const [key, value] of Object.entries(filter.relationships)) {
        options.facetFilters.push([key.replace(/[-_]/, '.'), value].join(':'));
      }
    }

    if (filter.limit) {
      options.hitsPerPage = filter.limit;
    }
    if (filter.page) {
      options.page = filter.page - 1;
    }

    return options;
  }

  protected transformWhereFilter(where: WhereFilter | WhereFilter[]): SearchOptions {
    const filters: string[] = [];
    const facetFilters: Array<string | string[]> = [];

    for (const [field, type] of Object.entries(where)) {
      if (toString.call(type) === '[object Object]') {
        for (const [operator, value] of Object.entries(type) as [string, any]) {
          switch (operator) {
            case 'eq':
              filters.push(`${field}:${JSON.stringify(value)}`);
              break;
            case 'neq':
              filters.push(`NOT ${field}:${JSON.stringify(value)}`);
              break;
            case 'gt':
              filters.push(`${field} > ${value}`);
              break;
            case 'gte':
              filters.push(`${field} >= ${value}`);
              break;
            case 'lt':
              filters.push(`${field} < ${value}`);
              break;
            case 'lte':
              filters.push(`${field} <= ${value}`);
              break;
            case 'inq':
              const inq = value.map(v => `${field}:${v}`).join(' OR ');
              facetFilters.push(`(${inq})`);
              break;
            case 'nin':
              const nin = value.map(v => `NOT ${field}:${JSON.stringify(v)}`).join(' AND ');
              filters.push(`(${nin})`);
              break;
            case 'between':
              filters.push(`${field}:${value[0]} TO ${value[1]}`);
              break;
            default:
              break;
          }
        }
      } else {
        facetFilters.push(`${field}:${type}`);
      }
    }

    const options: any = {};

    if (filters.length) {
      options.filters = filters.join(' AND ');
    }

    if (facetFilters.length) {
      options.facetFilters = facetFilters;
    }

    return options;
  }

  protected createResponse(res: SearchResponse): AxiosResponse {
    return {
      status: 200,
      statusText: 'Success.',
      data: {
        meta: {
          total: res.nbHits,
          totalPages: res.nbPages,
          page: res.page + 1,
          limit: res.hitsPerPage
        },
        data: res.hits
      }
    } as AxiosResponse;
  }
}
