A simple REST API Gateway that not simply forward
request to the server, but also add a real time support.

# Creating Gateway Server

## Simple Gateway
Simple gateway will simply create a server without
any custom routing and authentication. You can install
the `@packet/api-bridge` globally if you want to start a simple gateway.

### Installation

```bash
npm install -g @packet/api-bridge
```

Simple gateway will read `API_URL, SERVER_SECRET, and SERVER_PORT`
variable from the `process.env`. So make sure you define that
on your environment, either via `.env` file or `export` method.

```bash
API_URL=localhost:3000 SERVER_SECRET=@kkwer8444 SERVER_PORT=3000 api-bridge
```

### Subscribe to the real-time event.
```typescript
const websocket = new WebSocket(`ws://localhost:3000`);

websocket.onmessage = (message :string) => {
  console.log(message);
};

// Subscribe to /users path so will receive an event
// when something happened on the /users path.
websocket.send(JSON.stringify({
  type: 'subscribe',
  path: '/users' 
}));
```

After you start the gateway server and subscribe to the real time event,
when you do a `POST, PUT, PATCH` and `DELETE` request,
you will get the event and data via websocket.

## Advanced Gateway
Advanced Gateway will allow you to have more control
to the gateway such adding custom route and authentication.

### Installation
```bash
npm install --save @packet/api-bridge
```

### Creating Gateway Instance.
When creating a gateway instance, you need to specify
the secret word to be used by the EventDriver, and
add at least one Remote as a detination to forward
the requests.
```typescript
import { Gateway } from '@packet/api-bridge';

const gateway = new Gateway({
  secret: 'supersecretword'
});

gateway.addRemote({
  name: 'default',
  baseURL: 'https://api.server.com'
});
```

### Register JWT Authentication.
By default the JWT Authentication will encode all data
returned from the server to create a token.
```typescript
gateway.addAuth({
  jwtSecret: 'jwtsupersecret',
  authenticationPath: '/login'
});
```
If you want to create a custom token payload, like
if the server returns a full user data but you only
want to include `id` and `email` on the token, you
can add a function to create the payload on the auth config. 
```typescript
gateway.addAuth({
  jwtSecret: 'jwtsupersecret',
  authenticationPath: '/login',
  createProfile: (res: AxiosResponse) => {
    return { id: res.data.id, email: res.data.email };
  }
});
```
If you try `POST /login`, the response data will
have the data returned from the remote server and
includes `jwt` property contains the token.

After adding a JWT Authentication, you must connect
to the WebSocket after logging in, and provide a `token`
to the ws url.
```typescript
const ws = new WebSocket(`http://localhost:3000?token=${jwt}`);
```

### Starting Gateway Server.

```typescript
gateway.start(8000).then(() => {
  console.log('Server listening on port 8000.');
});
```

### Publishing Event From Outside
Sometimes event happened outside the `POST, PUT, PATCH` and `DELETE` request.
You can publish an event from outside the gateway by
adding a `eventHook` to the Gateway config.

Publishing an event from outside require to add a
`secret` to the query params.

```typescript
const gateway = new Gateway({
  // ...
  eventHook: '/publish-event'
});
```
After adding event hook, you can publish an event from outside.
```typescript
axios.post('http://localhost:3000/publish-event?secret=supersecret', {
  path:'/users', 
  method:'put', 
  data: {}
});
```

## Middleware
Middlware is a function that will be called before
forwarding the request to the remote. If the request should
not be forwarded to the remote, a middleare must
return an error so the gatway will reject the request.

When adding a middleware, you can add middleware for
bot http and websocket request.

```typescript
gateway.use({
  http: (req) => {
    if (!req.headers['Special']) return new MiddlewareErro(400, 'Bad request.');
  },
  socket: (ctx) => {
    if (!ctx.params.special) return new MiddlewareError(400, 'Bad request.');
  }
});
```

## Custom Route
Sometimes you want to bypass the middleware and
forward the request directly. For example, a `/signup`
path should be forwarded directly to the remote.

```typescript
gateway.route({
  path: '/signup',
  methods: ['post'],
  direct: true
});
```

If you want to add multiple routes at once, you can
simply use array of routes instead a aroute object.
```typescript
gateway.route([ { path, methods } ]);
```

If you want to restrict access to a path, you can
add a `security` to the route. For example, you only
to allow access to the logged in users.
```typescript
gateway.route({
  path: '/projects',
  methods: ['post', 'put', 'delete'],
  security: { type: AccessType.AUTHENTICATED }
});
```

If you want to a custom data to be returned to the
client, you can add `transformResponse` function
to the route.
```typescript
gateway.route({
  path: '/profile',
  methods: ['get'],
  transformResponse: async (res: AxiosResponse) => {
    const fb = await fb.profile();
    res.data.fbProfile = fb;
  }
});
```

## Multiple Remotes
If you have a micro services, then you want to 
have a single API Gateway that wrap all the services instead
requesting to a multiple APIs from the client side.

```typescript
gateway
  .addRemote({
    name: 'user-service',
    baseURL: 'https://user.server.com'
  })
  .addRemote({
    name: 'project-service',
    headers: { 'x-secret': 'anothersecretword' }
    baseURL: 'https://project.server.com'
  });

```

After we have a multiple remotes, then we can
tell the gateway how to swtich the remote to forward
the request.

**Using Header**
```typescript
axios.get('/users', {
  headers: {'x-remote':'user-service'}
});
```
**Using Route**
```typescript
gateway.route({
  path: '/users',
  methods: ['get', 'post', 'put', 'delete'],
  remote: 'user-service'
});
```
**Using Remote Config**
```typescript
gateway.addRemote({
  // ...
  switch: (req) => req.path.startsWith('/custom-service')
});
```

If there is a request that can't find the remote
because no `x-remote` header defined and no route defined,
the gateway will switch to the default remote.

First, the gateway will try to find a remote with `name: 'default'`.
If no remote with `default` name, then it'll use
the first registered remote as the default remote.

Some remote service maybe need a different request data. You
can add a function to create the request config to
the remote config.
```typescript
gateway.addRemote({
  name: 'profile-service',
  baseURL: 'https://profile.server.com',
  createConfig: (req: GatewayRequest) => {
    return {
      url: req.originalUrl,
      method: req.method,
      headers: {
        'Authorization': `Basic ${req.authProfile.basicToken}`
      }
    }
  }
});
```

## Custom Response
Sometimes you want to transform the response from
the remote to use a standard, so all data returned
from the gateway using the same format.

You can transform the response on the remote config
level, or on the route config level.

**Remote Level**
```typescript
gateway.addRemote({
  // ...
  transformResponse: (res: AxiosResponse) => {
    res.data = {
      data: res.data,
      meta: res.headers
    }
  }
});
```

**Route Level**
```typescript
gateway.route({
  path: '/users',
  transformResponse: (res: AxiosResponse) => {
    delete res.data.password;
  }
});
```

## Access Control
The gateway auth has a built in role based authentication.
To use that, the token payload must have a `roles: []` field. 
You also need to define a custom route to use the role based
authentication.

```typescript
gateway.route([
  {
    path: '/users',
    methods: ['post'],
    security: { type: AccessType.HAS_ROLE, roles: ['ADMIN'] },
  },
  {
    path: '/projects',
    methods: ['post'],
    security: { type: AccessType.HAS_ANY_ROLE, roles: ['ADMIN', 'PROJECT-MANAGER'] },
  },
  {
    path: '/system',
    methods: ['post'],
    security: { type: AccessType.DENNY_ALL },
  },
  {
    path: '/about',
    methods: ['post'],
    security: { type: AccessType.ALLOW_ALL },
  },
]);
```

## Custom Gateway
If you need to customize everything because your
API server is too complex, then you can create a custom
gateway that extends the `Gateway` class.

```typescript
class CustomGateway extends Gateway {
  protected error() {
    throw new Error('Custom error.');
  }
}

class CustomRemote extends BasicRemote {
  protected transformResponse() {
    // Logic to transform the response.
  }
}

const gateway = new CustomGateway(config);
gateway.addRemote(CustomRemote, remoteConfig);
gateway.start(8000);
```
