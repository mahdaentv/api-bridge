"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
class Remote {
}
exports.Remote = Remote;
class BasicRemote extends Remote {
    constructor(configs) {
        super();
        this.configs = configs;
        const { baseURL, headers } = configs;
        this.http = axios_1.default.create({ baseURL, headers });
    }
    async request(req) {
        const config = await this.createConfig(req);
        const response = await this.http.request(config);
        if (this.transformResponse) {
            await this.transformResponse(req, response);
        }
        if (this.configs.transformResponse) {
            await this.configs.transformResponse(req, response);
        }
        return response;
    }
    async createConfig(req) {
        if (this.configs.createConfig) {
            return this.configs.createConfig(req);
        }
        else {
            const headers = Object.assign({}, req.headers);
            delete headers.host;
            delete headers.connection;
            return {
                url: req.originalUrl,
                method: req.method,
                data: req.body,
                headers: this.configs.headers || headers
            };
        }
    }
}
exports.BasicRemote = BasicRemote;
//# sourceMappingURL=remote.js.map