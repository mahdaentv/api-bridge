"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AccessType;
(function (AccessType) {
    AccessType[AccessType["AUTHENTICATED"] = 0] = "AUTHENTICATED";
    AccessType[AccessType["HAS_ANY_ROLE"] = 1] = "HAS_ANY_ROLE";
    AccessType[AccessType["HAS_ROLE"] = 2] = "HAS_ROLE";
    AccessType[AccessType["ALLOW_ALL"] = 3] = "ALLOW_ALL";
    AccessType[AccessType["DENNY_ALL"] = 4] = "DENNY_ALL";
})(AccessType = exports.AccessType || (exports.AccessType = {}));
//# sourceMappingURL=interface.js.map