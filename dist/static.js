"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const gateway_1 = require("./gateway");
const { API_URL, SERVER_SECRET = 'nobodyknows', SERVER_PORT = 3000, JWT_SECRET, AUTH_PATH, } = process.env;
if (!API_URL) {
    throw new Error('Remote API_URL required.');
}
const gateway = new gateway_1.Gateway({ secret: SERVER_SECRET, eventHook: '/push-event' });
gateway.addRemote({
    name: 'default',
    baseURL: API_URL,
});
if (JWT_SECRET && AUTH_PATH) {
    gateway.addAuth({
        jwtSecret: JWT_SECRET,
        authenticationPath: AUTH_PATH,
        createProfile: (res) => {
            return { id: res.data.id };
        },
    });
}
function serve() {
    gateway
        .start(parseInt(SERVER_PORT))
        .then(() => {
        console.log(`Server listening on http://localhost:${SERVER_PORT}.`);
    });
}
exports.serve = serve;
//# sourceMappingURL=static.js.map