"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const algoliasearch_1 = __importDefault(require("algoliasearch"));
const remote_1 = require("./remote");
class Algolia extends remote_1.Remote {
    constructor(configs) {
        super();
        this.configs = configs;
        this.indexes = {};
        this.client = algoliasearch_1.default(configs.appId, configs.appKey);
    }
    async request(req) {
        const index = this.initIndex(req.headers['x-index']);
        const { query, options } = this.createRequest(req);
        console.log(query, options);
        const res = await index.search(query, options);
        const response = this.createResponse(res);
        if (this.transformResponse) {
            await this.transformResponse(req, response);
        }
        if (this.configs.transformResponse) {
            await this.configs.transformResponse(req, response);
        }
        return response;
    }
    switch(req) {
        return 'x-index' in req.headers;
    }
    initIndex(name) {
        const { prefix, suffix } = this.configs;
        return this.client.initIndex(`${prefix || ''}${name}${suffix || ''}`);
    }
    createRequest(req) {
        const { search, filter = {}, facets, relationRef } = req.query || {};
        if (typeof relationRef === 'object') {
            filter.relationships = relationRef;
        }
        return {
            query: search || '',
            options: this.createOptions(filter, facets)
        };
    }
    createOptions(filter, facets) {
        const options = {};
        if (facets) {
            options.facets = facets;
        }
        if (filter.where) {
            Object.assign(options, this.transformWhereFilter(filter.where));
        }
        if (filter.relationships) {
            if (!options.facetFilters) {
                options.facetFilters = [];
            }
            for (const [key, value] of Object.entries(filter.relationships)) {
                options.facetFilters.push([key.replace(/[-_]/, '.'), value].join(':'));
            }
        }
        if (filter.limit) {
            options.hitsPerPage = filter.limit;
        }
        if (filter.page) {
            options.page = filter.page - 1;
        }
        return options;
    }
    transformWhereFilter(where) {
        const filters = [];
        const facetFilters = [];
        for (const [field, type] of Object.entries(where)) {
            if (toString.call(type) === '[object Object]') {
                for (const [operator, value] of Object.entries(type)) {
                    switch (operator) {
                        case 'eq':
                            filters.push(`${field}:${JSON.stringify(value)}`);
                            break;
                        case 'neq':
                            filters.push(`NOT ${field}:${JSON.stringify(value)}`);
                            break;
                        case 'gt':
                            filters.push(`${field} > ${value}`);
                            break;
                        case 'gte':
                            filters.push(`${field} >= ${value}`);
                            break;
                        case 'lt':
                            filters.push(`${field} < ${value}`);
                            break;
                        case 'lte':
                            filters.push(`${field} <= ${value}`);
                            break;
                        case 'inq':
                            const inq = value.map(v => `${field}:${v}`).join(' OR ');
                            facetFilters.push(`(${inq})`);
                            break;
                        case 'nin':
                            const nin = value.map(v => `NOT ${field}:${JSON.stringify(v)}`).join(' AND ');
                            filters.push(`(${nin})`);
                            break;
                        case 'between':
                            filters.push(`${field}:${value[0]} TO ${value[1]}`);
                            break;
                        default:
                            break;
                    }
                }
            }
            else {
                facetFilters.push(`${field}:${type}`);
            }
        }
        const options = {};
        if (filters.length) {
            options.filters = filters.join(' AND ');
        }
        if (facetFilters.length) {
            options.facetFilters = facetFilters;
        }
        return options;
    }
    createResponse(res) {
        return {
            status: 200,
            statusText: 'Success.',
            data: {
                meta: {
                    total: res.nbHits,
                    totalPages: res.nbPages,
                    page: res.page + 1,
                    limit: res.hitsPerPage
                },
                data: res.hits
            }
        };
    }
}
exports.Algolia = Algolia;
//# sourceMappingURL=algolia.js.map