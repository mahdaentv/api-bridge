"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const _ = __importStar(require("lodash"));
function selectFields(data, fields) {
    if (Array.isArray(data)) {
        return data.map(d => selectFields(d, fields));
    }
    else {
        const selected = {};
        for (const [field, include] of Object.entries(fields)) {
            const value = _.get(data, field);
            if (include === true && value) {
                _.set(selected, field, value);
            }
            else if (include === false) {
                _.unset(data, field);
            }
            else if (toString.call(include) === '[object Object]' && value) {
                _.set(selected, field, selectFields(value, include));
            }
        }
        if (Object.keys(selected).length) {
            return selected;
        }
        else {
            return data;
        }
    }
}
exports.selectFields = selectFields;
function filterFields(data, req) {
    if (req.query.filter && req.query.filter.fields) {
        return selectFields(data, req.query.filter.fields);
    }
    else {
        return data;
    }
}
exports.filterFields = filterFields;
//# sourceMappingURL=utils.js.map