"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const remote_1 = require("./remote");
const auth_1 = require("./auth");
const rest_time_1 = require("./rest-time");
const event_bridge_1 = require("event-bridge");
class MiddlewareError extends Error {
    constructor(code, message) {
        super(message);
        this.code = code;
    }
}
exports.MiddlewareError = MiddlewareError;
class Gateway {
    constructor(configs) {
        this.configs = configs;
        this.remotes = {};
        this.routes = {};
        this.middlewares = [];
        this.app = new rest_time_1.RestTime(configs);
        if (configs.eventHook) {
            this.app.http.post(configs.eventHook, (req, res) => {
                const { path, type, data } = req.body;
                if (!path || !type || !data) {
                    res.status(400);
                    res.send({ errors: ['Bad request. Event must have { path, type, data }!'] });
                    return;
                }
                if (req.query.secret !== configs.secret) {
                    res.status(400);
                    res.send({ errors: ['Bad request. Query param ?secret={{GATEWAY_SECRET}} is required.'] });
                    return;
                }
                this.app.emit({ path, type, data });
                res.send('Published.');
            });
        }
    }
    addRemote(remote, configs) {
        if (remote instanceof remote_1.Remote) {
            this.remotes[remote.configs.name] = remote;
            if (remote.configs.name === 'default' || remote.configs.default) {
                this.defaultRemote = remote;
            }
        }
        else if (typeof remote === 'function') {
            this.remotes[configs.name] = new remote(configs);
            if (configs.name === 'default' || configs.default) {
                this.defaultRemote = this.remotes[configs.name];
            }
        }
        else {
            this.remotes[remote.name] = new remote_1.BasicRemote(remote);
            if (remote.name === 'default' || remote.default) {
                this.defaultRemote = this.remotes[remote.name];
            }
        }
        if (!this.defaultRemote) {
            for (const [, instance] of Object.entries(this.remotes)) {
                this.defaultRemote = instance;
            }
        }
        return this;
    }
    addAuth(auth, configs) {
        if (typeof auth === 'function') {
            this.addAuth(new auth(configs));
        }
        else if (auth instanceof auth_1.Auth) {
            this.use(auth.middleware());
            if (auth.route) {
                this.route(auth.route());
            }
        }
        else {
            this.addAuth(new auth_1.JWTAuth(configs));
        }
        return this;
    }
    use(middlware) {
        this.middlewares.push(middlware);
    }
    route(route) {
        if (Array.isArray(route)) {
            route.forEach(r => this.route(r));
        }
        else {
            this.routes[route.path] = route;
            for (const method of route.methods) {
                if (typeof this.app[method] === 'function') {
                    this.app[method](route.path, async (req, res) => {
                        for (const [key, value] of Object.entries(req.query)) {
                            if (typeof value === 'string' && ((value.startsWith('{') && value.endsWith('}')) ||
                                (value.startsWith('[') && value.endsWith(']')))) {
                                try {
                                    req.query[key] = JSON.parse(value);
                                }
                                catch (error) { }
                            }
                        }
                        try {
                            if (route.beforeRequest) {
                                await route.beforeRequest(req);
                            }
                            if (route.direct) {
                                return await this.directRequest(req, res, route.transformResponse);
                            }
                            else {
                                return await this.request(req, res, route.transformResponse);
                            }
                        }
                        catch (error) {
                            this.error(res, error);
                        }
                    });
                }
            }
        }
        return this;
    }
    start(port) {
        this.routeAll();
        return new Promise(resolve => {
            this.app.listen(port, resolve);
        });
    }
    async request(req, res, handler) {
        for (const middleware of this.middlewares) {
            const err = await middleware(req);
            if (err) {
                res.status(err.code);
                res.send({ errors: [err.message] });
                return;
            }
        }
        const response = await this.forward(req, res);
        if (response) {
            if (typeof handler === 'function') {
                await handler(req, response);
            }
            await this.response(req, res, response);
        }
    }
    async directRequest(req, res, handler) {
        const response = await this.forward(req, res);
        if (response) {
            if (typeof handler === 'function') {
                await handler(req, response);
            }
            await this.response(req, res, response);
        }
    }
    async forward(req, res) {
        const remote = this.switchRemote(req);
        if (remote) {
            try {
                return await remote.request(req);
            }
            catch (error) {
                this.error(res, error);
            }
        }
        else {
            res.status(500);
            res.send({ errors: ['No remote to handle the requests.'] });
        }
    }
    async response(req, res, response) {
        const { status, data, headers = {} } = response;
        res.status(status);
        for (const [key, val] of Object.entries(headers)) {
            res.setHeader(key, val);
        }
        res.send(data);
        if (['post', 'put', 'patch', 'delete'].includes(req.method.toLowerCase())) {
            if (req.method.toLowerCase() === 'delete' && !data.id) {
                const paths = req.path.split('/');
                const id = paths.pop();
                if (id && /[-]+/.test(id) && id.length > 30) {
                    this.app.emit({ type: req.method.toLowerCase(), path: req.path, data: { id } });
                }
            }
            else {
                this.app.emit({ type: req.method.toLowerCase(), path: req.path, data });
            }
        }
    }
    error(res, error) {
        event_bridge_1.Logger.error(error.message, error);
        if (error.response) {
            res.status(error.response.status);
            res.send(error.response.data);
        }
        else {
            res.status(500);
            res.send({ errors: [error.message] });
        }
    }
    routeAll() {
        if (!this.configs.routedOnly) {
            this.route({
                path: '/**',
                methods: ['get', 'post', 'put', 'patch', 'delete']
            });
        }
    }
    switchRemote(req) {
        const { path } = req;
        if (this.routes[path]) {
            const { remote } = this.routes[path];
            if (remote && this.remotes[remote]) {
                return this.remotes[remote];
            }
        }
        else {
            const remote = req.headers['x-remote'];
            if (remote && this.remotes[remote]) {
                return this.remotes[remote];
            }
        }
        for (const [name, remote] of Object.entries(this.remotes)) {
            if (typeof remote.switch === 'function' && remote.switch(req)) {
                return remote;
            }
            if (typeof remote.configs.switch === 'function' && remote.configs.switch(req)) {
                return remote;
            }
        }
        return this.defaultRemote;
    }
}
exports.Gateway = Gateway;
//# sourceMappingURL=gateway.js.map