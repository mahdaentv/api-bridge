import { ServerConfig } from 'event-bridge';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { Request } from 'express';
import { WhereFilter } from 'event-bridge';
export interface AuthProfile {
    id: string;
    roles?: string[];
}
export interface AuthConfig {
    jwtSecret: string;
    authenticationPath?: string;
    beforeLogin?: (req: GatewayRequest) => void;
    createProfile?: (res: AxiosResponse) => AuthProfile;
    afterLogin?: GatewayResponseHandler;
    transformRequest?: (req: GatewayRequest, profile: AuthProfile) => void;
}
export declare enum AccessType {
    AUTHENTICATED = 0,
    HAS_ANY_ROLE = 1,
    HAS_ROLE = 2,
    ALLOW_ALL = 3,
    DENNY_ALL = 4
}
export interface GatewayConfig extends ServerConfig {
    routedOnly?: boolean;
    eventHook?: string;
    secret?: string;
}
export interface GatewayRequest extends Request {
    authProfile: AuthProfile;
}
export declare type GatewayRequestHandler = (req: GatewayRequest) => void | Promise<void>;
export declare type GatewayResponseHandler = (req: GatewayRequest, res: AxiosResponse) => void | Promise<void>;
export declare type RouteMethod = 'post' | 'get' | 'put' | 'patch' | 'delete';
export declare type Route = {
    path: string;
    methods: Array<RouteMethod>;
    direct?: boolean;
    remote?: string;
    security?: RouteSecurity;
    beforeRequest?: GatewayRequestHandler;
    transformResponse?: GatewayResponseHandler;
};
export declare type RouteSecurity = {
    type: AccessType;
    roles?: string[];
};
export interface RemoteConfig {
    name: string;
    baseURL: string;
    default?: boolean;
    headers?: object;
    createConfig?: (req: GatewayRequest) => AxiosRequestConfig | Promise<AxiosRequestConfig>;
    transformResponse?: (req: GatewayRequest, res: AxiosResponse) => void | Promise<void>;
    switch?: (req: GatewayRequest) => boolean | void;
}
export declare type GatewayQuery = {
    search?: string;
    filter?: GatewayQueryFilter;
};
export declare type GatewayQueryFilter = {
    limit?: number;
    page?: number;
    where?: WhereFilter | WhereFilter[];
    facets?: string[];
    relationships: {
        [key: string]: string;
    };
};
export declare type Fields<T> = {
    [K in keyof T]?: boolean | Fields<T[K]>;
};
export declare type FieldSelector = Array<string | SubFieldSelector>;
export declare type SubFieldSelector = {
    key: string;
    fields: FieldSelector;
};
