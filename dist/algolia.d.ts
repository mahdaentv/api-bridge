import { SearchIndex } from 'algoliasearch';
import { SearchResponse, SearchOptions } from '@algolia/client-search';
import { Remote } from './remote';
import { GatewayQueryFilter, GatewayRequest, RemoteConfig } from './interface';
import { AxiosResponse } from 'axios';
import { WhereFilter } from 'event-bridge';
export interface AlgoliaConfig extends RemoteConfig {
    appId: string;
    appKey: string;
    prefix?: string;
    suffix?: string;
}
export interface AlgoliaRequest {
    query?: string;
    options?: SearchOptions;
}
export declare class Algolia extends Remote {
    configs: AlgoliaConfig;
    private client;
    private indexes;
    constructor(configs: AlgoliaConfig);
    request(req: GatewayRequest): Promise<AxiosResponse>;
    switch(req: GatewayRequest): boolean | void;
    transformResponse?(req: GatewayRequest, res: AxiosResponse): void | Promise<void>;
    protected initIndex(name: string): SearchIndex;
    protected createRequest(req: GatewayRequest): AlgoliaRequest;
    protected createOptions(filter: GatewayQueryFilter, facets?: string[]): SearchOptions;
    protected transformWhereFilter(where: WhereFilter | WhereFilter[]): SearchOptions;
    protected createResponse(res: SearchResponse): AxiosResponse;
}
