import { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { GatewayRequest, RemoteConfig } from './interface';
export declare abstract class Remote {
    configs: RemoteConfig;
    abstract request(req: GatewayRequest): Promise<AxiosResponse>;
    abstract switch?(req: GatewayRequest): boolean | void;
    abstract transformResponse?(req: GatewayRequest, res: AxiosResponse): void | Promise<void>;
}
export declare type RemoteConstructor = {
    new (...args: any[]): Remote;
};
export declare class BasicRemote extends Remote {
    configs: RemoteConfig;
    protected http: AxiosInstance;
    constructor(configs: RemoteConfig);
    request(req: GatewayRequest): Promise<AxiosResponse>;
    switch?(req: GatewayRequest): boolean | void;
    transformResponse?(req: GatewayRequest, res: AxiosResponse): Promise<void>;
    protected createConfig(req: GatewayRequest): Promise<AxiosRequestConfig>;
}
