"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const event_bridge_1 = require("event-bridge");
const express_1 = __importDefault(require("express"));
const http_1 = require("http");
class RestTime {
    constructor(config) {
        this.http = express_1.default();
        this.server = http_1.createServer(this.http);
        this.wstp = new event_bridge_1.Server(this.server, config);
        this.http.use(express_1.default.json());
    }
    use(middleware) {
        this.http.use(middleware);
        this.wstp.use(middleware);
        return this;
    }
    useHttp(middleware) {
        this.http.use(middleware);
        return this;
    }
    useWstp(middleware) {
        this.wstp.use(middleware);
        return this;
    }
    all(path, handler) {
        const fn = this.createHandler(handler);
        this.http.all(path, fn);
        this.wstp.all(path, fn);
        return this;
    }
    get(path, handler) {
        const fn = this.createHandler(handler);
        this.http.get(path, fn);
        this.wstp.get(path, fn);
        return this;
    }
    post(path, handler) {
        const fn = this.createHandler(handler);
        this.http.post(path, fn);
        this.wstp.post(path, fn);
        return this;
    }
    put(path, handler) {
        const fn = this.createHandler(handler);
        this.http.put(path, fn);
        this.wstp.put(path, fn);
        return this;
    }
    patch(path, handler) {
        const fn = this.createHandler(handler);
        this.http.patch(path, fn);
        this.wstp.patch(path, fn);
        return this;
    }
    delete(path, handler) {
        const fn = this.createHandler(handler);
        this.http.delete(path, fn);
        this.wstp.delete(path, fn);
        return this;
    }
    options(path, handler) {
        const fn = this.createHandler(handler);
        this.http.options(path, fn);
        this.wstp.options(path, fn);
        return this;
    }
    emit(event) {
        this.wstp.emit(event);
    }
    listen(port, handler) {
        this.server.listen(port, handler);
    }
    createHandler(handler) {
        return (req, res) => {
            return handler(req, res, (event) => {
                this.emit(event);
            });
        };
    }
}
exports.RestTime = RestTime;
//# sourceMappingURL=rest-time.js.map