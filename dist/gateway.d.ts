import { Response } from 'express';
import { RemoteConstructor, Remote } from './remote';
import { AuthConfig, GatewayConfig, GatewayRequest, GatewayResponseHandler, RemoteConfig, Route } from './interface';
import { AxiosError, AxiosResponse } from 'axios';
import { Auth, AuthConstructor } from './auth';
import { RestTime } from './rest-time';
export declare type GatewayMiddlware = (req: GatewayRequest, route?: Route) => void | MiddlewareError | Promise<void | MiddlewareError>;
export declare class MiddlewareError extends Error {
    code: number;
    constructor(code: number, message: string);
}
export declare class Gateway {
    private configs;
    private defaultRemote;
    private readonly remotes;
    private readonly routes;
    readonly middlewares: GatewayMiddlware[];
    readonly app: RestTime;
    constructor(configs: GatewayConfig);
    addRemote(remoteInstance: Remote): this;
    addRemote(remoteConfigs: RemoteConfig): this;
    addRemote(RemoteClass: RemoteConstructor, configs: RemoteConfig): this;
    addAuth(authInstance: Auth): this;
    addAuth(authConfigs: AuthConfig): this;
    addAuth(AuthClass: AuthConstructor, configs: AuthConfig): this;
    use(middlware: GatewayMiddlware): void;
    route(route: Route | Route[]): this;
    start(port: number): Promise<void>;
    protected request(req: GatewayRequest, res: Response, handler?: GatewayResponseHandler): Promise<void>;
    protected directRequest(req: GatewayRequest, res: Response, handler?: GatewayResponseHandler): Promise<void>;
    protected forward(req: GatewayRequest, res: Response): Promise<AxiosResponse>;
    protected response(req: GatewayRequest, res: Response, response: AxiosResponse): Promise<void>;
    protected error(res: Response, error: AxiosError): void;
    protected routeAll(): void;
    protected switchRemote(req: GatewayRequest): Remote;
}
