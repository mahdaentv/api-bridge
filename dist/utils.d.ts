import { Fields, GatewayRequest } from './interface';
export declare function selectFields<T>(data: object | object[], fields: Fields<T>): object | object[];
export declare function filterFields(data: object | object[], req: GatewayRequest): object | object[];
