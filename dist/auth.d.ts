import { AuthConfig, AuthProfile, GatewayRequest, Route } from './interface';
import { GatewayMiddlware, MiddlewareError } from './gateway';
export declare abstract class Auth {
    abstract middleware(): GatewayMiddlware;
    abstract route?(): Route | Route[];
}
export declare type AuthConstructor = {
    new (...args: any[]): Auth;
};
export declare class AccessError extends MiddlewareError {
    constructor();
}
export declare class BadAuthRequest extends MiddlewareError {
    constructor();
}
export declare class JWTAuth extends Auth {
    protected configs: AuthConfig;
    constructor(configs: AuthConfig);
    middleware(): GatewayMiddlware;
    route(path?: string): Route;
    protected createToken(profile?: AuthProfile): string;
    protected httpAuth(req: GatewayRequest, route?: Route): void | MiddlewareError;
    protected verifyAccess(req: GatewayRequest, route: Route, profile: AuthProfile): void | MiddlewareError;
}
