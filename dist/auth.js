"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const interface_1 = require("./interface");
const jsonwebtoken_1 = require("jsonwebtoken");
const gateway_1 = require("./gateway");
class Auth {
}
exports.Auth = Auth;
class AccessError extends gateway_1.MiddlewareError {
    constructor() {
        super(401, 'Unauthorized access.');
    }
}
exports.AccessError = AccessError;
class BadAuthRequest extends gateway_1.MiddlewareError {
    constructor() {
        super(400, 'Bad authorization request.');
    }
}
exports.BadAuthRequest = BadAuthRequest;
class JWTAuth extends Auth {
    constructor(configs) {
        super();
        this.configs = configs;
    }
    middleware() {
        return (req, route) => this.httpAuth(req, route);
    }
    route(path) {
        if (!path && !this.configs.authenticationPath) {
            throw new Error('No authentication path defined.');
        }
        return {
            path: path || this.configs.authenticationPath,
            methods: ['post'],
            direct: true,
            beforeRequest: (req) => {
                if (this.configs.beforeLogin) {
                    this.configs.beforeLogin(req);
                }
            },
            transformResponse: async (req, res) => {
                const profile = this.configs.createProfile ? this.configs.createProfile(res) : res.data;
                res.data = Object.assign(Object.assign({}, res.data), { jwt: this.createToken(profile) });
                if (this.configs.afterLogin) {
                    await this.configs.afterLogin(req, res);
                }
            }
        };
    }
    createToken(profile) {
        return jsonwebtoken_1.sign(profile, this.configs.jwtSecret);
    }
    httpAuth(req, route) {
        const { authorization, Authorization } = req.headers;
        if (route) {
            if (route.security && route.security.type === interface_1.AccessType.ALLOW_ALL) {
                return;
            }
            if (route.security && route.security.type === interface_1.AccessType.DENNY_ALL) {
                return new AccessError();
            }
        }
        if (authorization || Authorization) {
            const token = (authorization || Authorization).replace('Bearer ', '');
            const verified = jsonwebtoken_1.verify(token, this.configs.jwtSecret);
            if (verified) {
                const decoded = jsonwebtoken_1.decode(token);
                req.authProfile = decoded;
                if (this.configs.transformRequest) {
                    this.configs.transformRequest(req, decoded);
                }
                if (route && route.security) {
                    return this.verifyAccess(req, route, decoded);
                }
            }
            else {
                return new AccessError();
            }
        }
        else {
            return new BadAuthRequest();
        }
    }
    verifyAccess(req, route, profile) {
        const { type, roles } = route.security;
        if (interface_1.AccessType.AUTHENTICATED) {
            return;
        }
        if (type === interface_1.AccessType.HAS_ROLE) {
            if ((profile.roles || []).join(',') !== (roles || []).join(',')) {
                return new AccessError();
            }
        }
        if (type === interface_1.AccessType.HAS_ANY_ROLE) {
            let hasRole = false;
            for (const role of (roles || [])) {
                if ((profile.roles || []).includes(role)) {
                    hasRole = true;
                }
            }
            if (!hasRole) {
                return new AccessError();
            }
        }
    }
}
exports.JWTAuth = JWTAuth;
//# sourceMappingURL=auth.js.map