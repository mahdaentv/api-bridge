/// <reference types="node" />
import { RequestMiddleware, Server, ServerConfig, ServerEvent, Request as SocketRequest, Response as SocketResponse } from 'event-bridge';
import { Express, Request as HttpRequest, RequestHandler as HttpHandler, Response as HttpResponse } from 'express';
import { Server as HttpServer } from 'http';
export declare type Request<R> = HttpRequest & SocketRequest<R>;
export declare type Response<D> = HttpResponse & SocketResponse<D>;
export declare type Middleware<R, D> = (req: Request<R>, res: Response<D>) => Promise<void> | void;
export declare type RequestHandler<R, D> = (req: Request<R>, res: Response<D>, emit: Emitter<D>) => Promise<void> | void;
export declare type Emitter<D> = (event: ServerEvent<D>) => void;
export declare class RestTime {
    readonly http: Express;
    readonly wstp: Server;
    readonly server: HttpServer;
    constructor(config: ServerConfig);
    use<R, D>(middleware: Middleware<R, R>): this;
    useHttp(middleware: HttpHandler): this;
    useWstp(middleware: RequestMiddleware): this;
    all<R, D>(path: string, handler: RequestHandler<R, D>): this;
    get<D>(path: string, handler: RequestHandler<null, D>): this;
    post<R, D>(path: string, handler: RequestHandler<R, D>): this;
    put<R, D>(path: string, handler: RequestHandler<R, D>): this;
    patch<R, D>(path: string, handler: RequestHandler<R, D>): this;
    delete<D>(path: string, handler: RequestHandler<null, D>): this;
    options(path: string, handler: RequestHandler<null, null>): this;
    emit<T>(event: ServerEvent<T>): void;
    listen(port: number, handler?: () => void): void;
    protected createHandler<R, D>(handler: RequestHandler<R, D>): (req: Request<R>, res: Response<D>) => void | Promise<void>;
}
